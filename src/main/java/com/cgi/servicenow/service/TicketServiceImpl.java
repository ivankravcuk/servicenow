package com.cgi.servicenow.service;

import com.cgi.servicenow.entity.Ticket;
import com.cgi.servicenow.repository.TicketRepository;
import com.cgi.servicenow.rest.model.TicketDTO;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public TicketDTO createTicket(TicketDTO ticketDTO) {

        final Ticket entity = new Ticket(
                ticketDTO.getIdPersonAssigned(),
                ticketDTO.getIdPersonCreator(),
                ticketDTO.getName(),
                ticketDTO.getEmail(),
                LocalDateTime.now(Clock.systemUTC().withZone(ZoneId.of("Europe/Prague")))
        );

        Ticket managedEntity = ticketRepository.save(entity);

        return toTicketDTO(managedEntity);
    }

    @Override
    public TicketDTO findByIdTicket(Long id) {
        try {
            return toTicketDTO(ticketRepository.findByIdTicket(id));
        } catch (NullPointerException e) {
            throw new ServiceException("Ticket ID is null");
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<TicketDTO> findAll(Pageable pageable, String name) {
        Page<Ticket> entities;

        if (name == null || name.isEmpty()) {
            entities = ticketRepository.findAll(pageable);
        } else {
            entities = ticketRepository.findByName(name, pageable);
        }

        try {
            return entities.map(this::toTicketDTO);
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public TicketDTO deleteByIdTicket(Long id) {

        TicketDTO ticketDTO;

        try {
            ticketDTO = toTicketDTO(ticketRepository.findByIdTicket(id));
            ticketRepository.deleteByIdTicket(id);
        } catch (NullPointerException e) {
            throw new ServiceException("Ticket ID is null");
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }

        return ticketDTO;
    }


    private TicketDTO toTicketDTO(Ticket t) {
        return new TicketDTO(
                t.getIdTicket(),
                t.getIdPersonAssigned(),
                t.getIdPersonCreator(),
                t.getName(),
                t.getEmail(),
                t.getCreationDatetime()
        );
    }


}
