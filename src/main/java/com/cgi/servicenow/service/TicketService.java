package com.cgi.servicenow.service;

import com.cgi.servicenow.rest.model.TicketDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface TicketService {

    TicketDTO createTicket(TicketDTO ticketDTO);

    TicketDTO findByIdTicket(Long id);

    Page<TicketDTO> findAll(Pageable pageable, String name);

    TicketDTO deleteByIdTicket(Long id);
}
