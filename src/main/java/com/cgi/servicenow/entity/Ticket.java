package com.cgi.servicenow.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ticket")
@SequenceGenerator(
        name = Ticket.SEQUENCE_GENERATOR_NAME,
        sequenceName = "address_id_address_seq"
)
@Builder
@Entity
public class Ticket {

    static final String SEQUENCE_GENERATOR_NAME = "ticket_generator";

    @Id
    @GeneratedValue(generator = SEQUENCE_GENERATOR_NAME)
    @Column(name = "id_ticket", updatable = false, nullable = false)
    private Long idTicket;
    @Column(name = "id_person_assigned", nullable = true)
    private Long idPersonAssigned;
    @Column(name = "id_person_creator", nullable = false)
    private Long idPersonCreator;
    @Column(nullable = true, length = 50)
    private String name;
    @Column(nullable = true, length = 50)
    private String email;
    @Column(name = "creation_datetime", nullable = false)
    private LocalDateTime creationDatetime;

    public Ticket(Long idPersonAssigned, Long idPersonCreator, String name, String email, LocalDateTime creationDatetime) {
        this.idPersonAssigned = idPersonAssigned;
        this.idPersonCreator = idPersonCreator;
        this.name = name;
        this.email = email;
        this.creationDatetime = creationDatetime;
    }
}
