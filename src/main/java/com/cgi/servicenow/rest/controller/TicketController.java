package com.cgi.servicenow.rest.controller;

import com.cgi.servicenow.rest.exceptions.ResourceNotFoundException;
import com.cgi.servicenow.rest.model.TicketDTO;
import com.cgi.servicenow.service.ServiceException;
import com.cgi.servicenow.service.TicketService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "/tickets", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(path = "/tickets")
public class TicketController {

    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }


    @PostMapping
    public ResponseEntity<TicketDTO> createTicket(@RequestBody @Valid final TicketDTO ticketDTO) {
        return ResponseEntity.ok(ticketService.createTicket(ticketDTO));
    }

    @ApiOperation(
            value="Get ticket by Id",
            produces = "json",
            httpMethod = "GET",
            response = TicketDTO.class,
            nickname = "findTicketById"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "The requested resource was not found")
    })
    @GetMapping(path="/{id}")
    public ResponseEntity<TicketDTO> findTicketById(
            @ApiParam(value="ID of the ticket")
            @PathVariable("id") Long id) {

        try {
            return ResponseEntity.ok(ticketService.findByIdTicket(id));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @ApiOperation(
            httpMethod = "GET",
            value = "Get All Tickets",
            response = TicketDTO.class,
            responseContainer = "Page",
            nickname = "findAllTickets",
            produces = "json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "The requested resource was not found.")
    })
    @GetMapping
    public ResponseEntity<Page<TicketDTO>> findAllTickets(Pageable pageable,
                                                          @ApiParam(value="name of ticket")
                                                          @RequestParam(required = false) String name) {
        try {
            return ResponseEntity.ok(ticketService.findAll(pageable,name));
        }  catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<TicketDTO> deleteById(@PathVariable("id") Long id){
        try {
            return ResponseEntity.ok(ticketService.deleteByIdTicket(id));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }
}
