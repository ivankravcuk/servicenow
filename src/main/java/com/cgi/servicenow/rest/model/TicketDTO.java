package com.cgi.servicenow.rest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(value = "TicketDTO", description = "Information about ticket")
public class TicketDTO {

    @ApiModelProperty(value = "Ticket ID", example = "1")
    private Long ticketId;

    @ApiModelProperty(value = "ID of person assigned", example = "2")
    private Long idPersonAssigned;

    @ApiModelProperty(value = "ID of creator of the ticket", example = "3")
    private Long idPersonCreator;

    @ApiModelProperty(value = "Name of the ticket", example = "work")
    private String name;

    @ApiModelProperty(value = "Email of the creator", example = "mom@gmail.com")
    private String email;

    @ApiModelProperty(value = "Creation time of ticket", example = "2020-09-23T10:54:33.204")
    private LocalDateTime creationDatetime;
}
