package com.cgi.servicenow.repository;

import com.cgi.servicenow.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Ticket findByIdTicket(final Long id);

    Page<Ticket> findByName(final String name, Pageable pageable);

    Page<Ticket> findAll(Pageable pageable);

    void deleteByIdTicket(final Long id);
}
