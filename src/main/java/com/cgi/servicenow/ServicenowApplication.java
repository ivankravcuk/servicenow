package com.cgi.servicenow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Clock;
import java.time.LocalDateTime;


@SpringBootApplication
public class ServicenowApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicenowApplication.class, args);
    }

}
